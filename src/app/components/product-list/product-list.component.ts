import { Component, OnInit } from '@angular/core';
import { ProductService } from "src/app/services/product.service";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {

  products: any;

  constructor(private productService: ProductService) { }

  ngOnInit(): void {
     this.readProducts();
  }

readProducts(): void{
    this.productService.readAll()
    .subscribe(
      products => {
        this.products = products;
      },
      error => {
        console.log(error);
      });
  }

  refresh():void {
    this.readProducts();
  }

deleteProduct(id: string) {
    this.productService.delete(id)
      .subscribe(res => {
        console.log(res);
      },
      err => {
        console.log(err);
      });
  }
}
